require "./lib/client/api_request"
class Potepan::SuggestsController < ApplicationController
  def index
    response = Client::ApiRequest.suggest(params[:keyword], params[:max_num])
    if response.status == 200
      render json: response.body
    else
      render json: response.body, status: response.status
    end
  end
end
