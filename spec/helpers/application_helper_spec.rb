require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:base_title) { "BIGBAG Store" }

  it "display page title when refer to nothing" do
    expect(full_title("")).to eq base_title
    expect(full_title(nil)).to eq base_title
  end

  it "display page title when having product.name " do
    expect(full_title("test")).to eq "test - #{base_title}"
  end
end
