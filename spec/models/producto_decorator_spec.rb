require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon1)   { create(:taxon) }
  let(:taxon2)   { create(:taxon) }
  let(:taxon3)   { create(:taxon) }
  let(:product1) { create(:product, taxons: [taxon1, taxon2]) }
  let(:product2) { create(:product, taxons: [taxon1]) }
  let(:product3) { create(:product, taxons: [taxon2]) }
  let(:product4) { create(:product, taxons: [taxon3]) }

  it "related_products method return correct products" do
    expect(product1.related_products).to match_array [product2, product3]
  end
end
