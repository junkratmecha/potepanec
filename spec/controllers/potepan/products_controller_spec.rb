require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product)          { create(:product) }
  let(:taxon)            { create(:taxon) }
  let(:related_products) { create_list(:product, 2, taxons: [taxon]) }

  before do
    get :show, params: { id: product.id }
  end

  it "responds successfully" do
    expect(response).to be_successful
    expect(response).to have_http_status "200"
  end

  it "render show page" do
    expect(response).to render_template :show
  end

  it "assigns @product" do
    expect(assigns(:product)).to eq product
  end

  it 'assigns @related_products' do
    expect(assigns(:related_products)).to match_array related_products
  end
end
