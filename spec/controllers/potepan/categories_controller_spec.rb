require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product)  { create(:product, taxons: [taxon]) }

  before do
    get :show, params: { id: taxon.id }
  end

  it "responds successfully" do
    expect(response).to be_successful
    expect(response).to have_http_status "200"
  end

  it "render show page" do
    expect(response).to render_template :show
  end

  it "assigns @taxon" do
    expect(assigns(:taxon)).to eq taxon
  end

  it "assigns @taxonomies" do
    expect(assigns(:taxonomies)).to eq [taxonomy]
  end

  it "assigns @products" do
    expect(assigns(:products)).to eq [product]
  end
end
