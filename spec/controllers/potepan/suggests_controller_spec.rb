require 'rails_helper'

RSpec.describe Potepan::SuggestsController, type: :controller do
  let(:url) { ENV['API_URL'] }

  before do
    stub_request(:get, url).
      with(
        headers: { Authorization: "Bearer #{ENV['AUTHORIZATION_KEY']}" },
        query: { 'keyword' => 't', 'max_num' => 5 }
      ).
      to_return(
        body: response_body,
        status: response_status
      )
    get :index, params: { keyword: 't', max_num: 5 }
  end

  context "sucess case(200)" do
    let(:response_body) { ["test1", "test2", "test3"] }
    let(:response_status) { 200 }

    it "return correct body" do
      expect(JSON.parse(response.body)).to eq response_body
    end

    it "return correct response-status" do
      expect(response_status).to eq 200
    end
  end

  context "server error etc" do
    let(:response_body) { "error" }
    let(:response_status) { 500 }

    it "return error" do
      expect(response_body).to eq "error"
    end

    it "return error-status(500)" do
      expect(response_status).to eq response_status
    end
  end
end
