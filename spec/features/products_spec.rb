require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:product)             { create(:product, taxons: [taxon]) }
  let!(:taxon)               { create(:taxon) }
  let!(:taxon2)              { create(:taxon) }
  let!(:related_products)    { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_product) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "has products name in lightSection" do
    within find('.lightSection') do
      expect(page).to have_content product.name
    end
  end

  it "display products information" do
    within find('.singleProduct') do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  context "for related_products" do
    it "have related_products information" do
      within find('.productsContent') do
        expect(page).to have_content related_products.first.name
        expect(page).to have_content related_products.first.display_price
      end
    end

    it "do not have not_related_products information" do
      within find('.productsContent') do
        expect(page).to have_no_content not_related_product.name
      end
    end

    it "display 4 related_products when they are 5" do
      within find('.productsContent') do
        expect(page).to have_selector ".productBox", count: 4
      end
    end
  end
end
