require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: 'test_taxonomy') }
  let!(:taxon)    { create(:taxon, name: "test_taxon", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxon2)   { create(:taxon, name: "test_taxon2", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product)  { create(:product, name: "test_product", taxons: [taxon]) }
  let!(:product2) { create(:product, name: "test_product2", taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "has taxons name in light_upper" do
    within find('.breadcrumb') do
      expect(page).to have_content taxon.name
    end
  end

  it "display correct categories page" do
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content taxon2.name
    end
  end

  it "display correct products page" do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  it "do not display wrong products page" do
    within '.productBox' do
      expect(page).to have_no_content product2.name
    end
  end

  it "move category page from category name" do
    click_on taxon2.name
    expect(current_path).to eq potepan_category_path(taxon2.id)
  end

  it "move product page by product name" do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
