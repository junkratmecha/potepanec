require "httpclient"
module Client
  class ApiRequest
    def self.suggest(keyword, max_num)
      url = ENV['API_URL']
      query = { keyword: keyword, max_num: max_num }
      headers = { Authorization: "Bearer #{ENV['AUTHORIZATION_KEY']}" }
      HTTPClient.get(url, query, headers)
    end
  end
end
